package myutils

import (
	"io"
	"log"
	"net"
	"os"
)

// FileTransfer transfer file through *net.TCPConn
func FileTransfer(filePath string, conn *net.TCPConn) {
	fp, _ := os.OpenFile(filePath, os.O_RDONLY, 0777)

	buffer := make([]byte, 10)

	for {
		c, e := fp.Read(buffer)
		if e != nil {
			if e == io.EOF {
				_, err := conn.Write(buffer[0:c])
				if err != nil {
					log.Println("-> Send data error :", err)
				} else {
					log.Println("-> Send data successfully")
				}
				log.Println("End of file")
			} else {
				log.Println("Error while reading file :", err)
			}
			break
		}

		_, err := conn.Write(buffer[0:c])
		if err != nil {
			log.Println("-> Send data error :", err)
		} else {
			// log.Println("-> Send data successfully")
		}
	}

	fp.Close()
}
